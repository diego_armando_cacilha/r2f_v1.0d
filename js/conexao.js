/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/
$(document).ready(function (){
   $("#loginForm").submit(function (evt){
       evt.preventDefault();
       
       $.ajax({
            type: 'POST',
            url: "conexao.php",
            dataType: 'json',
            data: $("#loginForm").serialize(),
            beforeSend: function (xhr) {
                //alert("Antes de Enviar");
            },
            complete: function (jqXHR, textStatus) {
                //alert("Completo");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Usuário e/ou senha incorretos");  
            },
            success: function (data) {
                if(data.conn)
                    $(window.document.location).attr('href',data.url);
            }
       });
   }); 
}); 
//function login(){
//    $.ajax({
//        method: "post",
//        url: "conexao.php",
//        data: $("#meuForm").serialize(),
//        success: function(data){
//               alert(data);
//        }
//
//    });
//}

